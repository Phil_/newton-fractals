#include <iostream>
#include <complex>
#include <cmath>
#include <vector>
#include <functional>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


// TODO make this cuda compatbile

using std::cout;
using std::endl;
typedef std::complex<double> complex;

// -------------------------------------------------- //
// Configsection

// matrix resolution
int n_cols = 1920;
int n_rows = 1080;

// real part range
double start_real = -2.;
double end_real = 2.;

// imag part range
double start_complex = start_real / ( (n_cols  * 1.0) / n_rows);
double end_complex = end_real / ( (n_cols * 1.0) / n_rows);

// function and derivative
complex func(complex x) {
    return std::sin(complex(2) * x - std::log(x + complex(1))) * std::pow(x, 2) + complex(1);
    // return std::pow(x, 4) + complex(4) * std::pow(x, 5) - std::pow(x, 9) + complex(10);
}
complex func_(complex x) {
    return x * (complex(2) * std::sin(complex(2) * x - std::log(x + complex(1))) + (x * (complex(2) * x + complex(1)) * std::cos(complex(2) * x - std::log(x + complex(1))))/(x + complex(1)));
    // return complex(4) * std::pow(x, 3) + complex(20) * std::pow(x, 4) - complex(9) * std::pow(x, 8);
}

const int MAX_ITS = 1000;

// -------------------------------------------------- //


// function and derivative
complex func(complex);
complex func_(complex);
int calc(std::function<complex(complex)>, std::function<complex(complex)>, complex);

int main() {
    cout << start_real << " to " << end_real << " in " << n_cols << " steps" << endl;
    cout << start_complex << " to " << end_complex << " in " << n_rows << " steps" << endl;

    std::vector<int> n_its(n_rows*n_cols);
    cv::Mat mat(n_rows, n_cols, CV_64FC1);

    for (int i = 0; i < n_rows; ++i) {
        for (int j = 0; j < n_cols; ++j) {
            n_its[i * n_cols + j] = calc(func, func_, complex(
                start_real + j * (end_real-start_real)/n_cols,               // real -> x in matrix
                start_complex + i * (end_complex - start_complex)/n_rows     // imag -> y in matrix
            ));
        }
    }
    int max = *std::max_element(n_its.begin(), n_its.end());

    for (int i = 0; i < n_rows; ++i) {
        for (int j = 0; j < n_cols; ++j) {
            mat.at<double>(i, j) = n_its[i * n_cols + j] / (max * 1.0);
        }
    }

    // namedWindow( "Display window", cv::WINDOW_AUTOSIZE ); // Create a window for display.
    // imshow( "Display window", mat );
    // cv::waitKey(0);
    mat.convertTo(mat, CV_8UC3, 255.0);
    imwrite("fractal.png", mat);
    return 0;
}

int calc(std::function<complex(complex)> f, std::function<complex(complex)> f2, complex c) {
    int n = 0;
    complex delta = f(c) / f2(c);
    while (std::abs(delta) > 10e-2 && n < MAX_ITS) {
        c -= delta; ++n;
        delta = f(c) / f2(c);
    }
    return (n < MAX_ITS ? n : 0);
}
